<h1 align=center> FuchsiaOS </h1>

#### Why specifically [fuchsiaOS](https://www.quora.com/Why-is-Google-making-Fuchsia)? 
Fuchsia is not Linux
cross-platform OS
A modular, capability-based operating system
Improved synchronization + simpler navigation across devices
More streamlined app design
Deeper, more powerful voice control
smart home devices


#### There are so many open-source OSes to choose from — which aspect of fuchsiaOS makes you want to contribute to it?
Google a second chance to build a more secure, easily updated OS to enable even better cross-platform integration than the current Chrome OS / Android divide.
the Fuchsia team’s goal is nothing less than creating a single, unifying operating system that could run on all of Google’s devices


#### What is especially cool about it? And what do you want to fix or extend in it?  
Distribution & Syncronization amoung devices
Zircon Kernel
UI interface Flutter (UI toolkit)
Intelligence: Agent Framework & Suggestions

---

<h1 align=center> KayOS </h1>

## Links:

- https://www.higgypop.com/news/self-aware-operating-system/
- https://www.ijltet.org/journal/151063991811.pdf
- https://en.wikipedia.org/wiki/Artificial_intelligence_systems_integration
- https://dribbble.com/shots/3704963-Natural-OS-most-advanced-AI-operating-system
- https://www.researchgate.net/publication/320173364_Artificial_Intelligence_Operating_System
- https://algorithmxlab.com/blog/2018/07/03/china-to-create-the-the-worlds-first-ai-operating-system/
- https://www.quora.com/Is-there-any-artificial-intelligence-operating-system
