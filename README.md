<h1 align=center> KayAI </h1>
<h4 align=center> My Journey to become a better Engineer 👨‍💻 </h4>


## Courses

- **MIT 6.006 Introduction to Algorithms**
	- [x] Unit 1 - Introduction
		- [x] Lecture 1 (Algorithmic Thinking)
		- [x] Lecture 2 (Models of Computation)
	- [ ] Unit 2 - Sorting and Trees
		- [x] Lecture 3 (Insertion Sort, Merge Sort)
		- [x] Lecture 4 (Heaps and Heap Sort)
		- [ ] Lecture 5 (Binary Search Trees, BST Sort)

- **Mathematics for Machine Learning Specialization**
	- _**Linear Algebra**_
		- [x] Week 1
		- [ ] Week 2

- **Machine Learning**
	- [x] Week1 (Introduction)
	- [x] Week2 (Linear Regression with Multiple Variables)
	- [x] Week3 (Logistic Regression)
	- [ ] Week4 (Neural Networks Representation)

- **Machine Learning**
	- _**Machine Learning Foundations: A Case Study Approach**_
		- [ ] Week 1

- **Advanced Machine Learning Specialization**
	- _**Introduction to Deep Learning**_
		- [x] Week 1
		- [ ] Week 2

- **Deep Learning Specialization**
	- [x] _**Neural Networks and Deep Learning**_





## Books

- **Introduction to Algorithms**
	- _**I) Foundations**_
		- [x] Chapter 1: The Role of Algorithms in Computing
		- [ ] Chapter 2: Getting Started

- **The Go Programming Language**
	- [x] Chapter 1: Tutorial
	- [ ] Chapter 2: Program Structure

- **Artificial Intelligence - A Modern Approach**
	- _**I) Artificial Intelligence**_
		- [x] Chapter 1: Introduction
		- [x] Chapter 2: Intelligent Agents
	- _**II) Problem-solving**_
		- [ ] Chapter 3: Solving Problems by Searching

- **Hands-On Machine Learning with Scikit-Learn & TensorFlow**
	- _**I) The Fundamentals of Machine Learning**_
		- [x] Chapter 1: The Machine Learning Landscape
		- [x] Chapter 2: End-to-End Machine Learning Project
		- [x] Chapter 3: Classification
		- [ ] Chapter 4: Training Models